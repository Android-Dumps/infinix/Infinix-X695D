#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_Infinix-X695D.mk

COMMON_LUNCH_CHOICES := \
    lineage_Infinix-X695D-user \
    lineage_Infinix-X695D-userdebug \
    lineage_Infinix-X695D-eng
