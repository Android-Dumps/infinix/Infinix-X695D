#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from Infinix-X695D device
$(call inherit-product, device/infinix/Infinix-X695D/device.mk)

PRODUCT_DEVICE := Infinix-X695D
PRODUCT_NAME := lineage_Infinix-X695D
PRODUCT_BRAND := Infinix
PRODUCT_MODEL := Infinix X695D
PRODUCT_MANUFACTURER := infinix

PRODUCT_GMS_CLIENTID_BASE := android-transsion

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="sys_tssi_64_infinix-user 11 RP1A.200720.011 162655 release-keys"

BUILD_FINGERPRINT := Infinix/X695D-OP/Infinix-X695D:11/RP1A.200720.011/210911V82:user/release-keys
