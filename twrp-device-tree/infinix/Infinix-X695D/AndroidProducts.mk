#
# Copyright (C) 2022 The Android Open Source Project
# Copyright (C) 2022 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_Infinix-X695D.mk

COMMON_LUNCH_CHOICES := \
    omni_Infinix-X695D-user \
    omni_Infinix-X695D-userdebug \
    omni_Infinix-X695D-eng
